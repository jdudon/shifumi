# Projet de SHIFUMI  

## PHP 

### Objectif
#### Enoncé
- Créer un algo qui permettra à un utilisateur de choisir entre 3 boutons "Pierre" "Feuille" "Ciseaux"
- Stocker une valeur pour l'ordinateur (pour commencer il joue toujours la même chose). Si l'un ou l'autre gagne echo "Gagné!" sinon "Perdu"
- +L'ordinateur choisit également aléatoirement
- ++Ensuite créer un systeme de manches et stocker les données des manches
- (qui a gagné? en jouant quoi? combien de parties gangées?).
- +++Pour remporter un manche il faut avoir gagné 3 parties. Le jeu se joue en 3 manches.
- ++++A la fin de la partie afficher les scores et les stats du joueur et d l'ordinateur.

_Les + sont des bonus à réaliser petit à petit. Ca va de bonus simples à des bonus compliqué à mettre en place._

**N'oubliez pas que ceci est un exemple parmis d'autre de code pour crcéer l'algo dont on a besoin pour réliser notre projet. Faire différemment ne veut pas dire mal faire.**
