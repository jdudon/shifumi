<?php
session_start();
// On lnce la session (on essaie de toujours mettre le session_start() directement après l'ouverture de balise.)

// SHIFUMI
// Créer un algo qui permettra à un utilisateur de choisir entre 3 boutons "Pierre" "Feuille" "Ciseaux"
// Stocker une valeur pour l'ordinateur (pour commencer il joue toujours la même chose). Si l'un ou l'autre gagne echo "Gagné!" sinon "Perdu"
// +L'ordinateur choisit également aléatoirement
// ++Ensuite créer un systeme de manches et stocker les données des manches
// (qui a gagné? en jouant quoi? combien de parties gangées?).
// +++Pour remporter un manche il faut avoir gagné 3 parties. Le jeu se joue en 3 manches.
// ++++A la fin de la partie afficher les scores et les stats du joueur et d l'ordinateur.


$pierre = "pierre";
$feuille = "feuille";
$ciseaux = "ciseaux";
$choices = [$pierre, $feuille, $ciseaux];
$i = rand(0, 2);

//#Pour la session
// Je véririfie l'état de mes valeurs stockées dans win_tours_ordi, usr et les nb de tours pour le système de manches
// Je fait des conditions dans des conditions car il y plusieurs choses à prendre en compte : 
// - Si le nombre de tours gagnés par l'ordi ou l'utilisateur arrive au moins à deux ou qu'on a passé 3 tours alors le but est 
// de passer à la manche suivante donc je décrémente nb_manches et je reset les valeurs qui servent à compter.
 
if ($_SESSION['win_tours_ordi']  >= 2 || $_SESSION['win_tours_usr'] >= 2 || $_SESSION['nb_tours'] == 0) {
    $_SESSION['nb_manches']--;
    $_SESSION['win_tours_usr'] = 0;
    $_SESSION['win_tours_ordi'] = 0;
    $_SESSION['nb_tours'] = 3;
// - Si le nombre de manches tombe à 0  ou que le nombres de manches gagnées par l'un ou l'autre des joueurs est 
//d'au moins 2 alors la partie est finie et on réinitialise toutes les clés de $_SESSION pour repartir sur une base neuve.
    if ($_SESSION['nb_manches'] == 0 || $_SESSION['win_manches_ordi'] >= 2 || $_SESSION['win_manches_usr'] >= 2 ) {
        $_SESSION['nb_manches'] = 3;
        $_SESSION['nb_tours'] = 3;
        $_SESSION['win_manches_ordi'] = 0;
        $_SESSION['win_manches_usr'] = 0;
        $_SESSION['win_tours_ordi'] = 0;
        $_SESSION['win_tours_usr'] = 0;
        $_SESSION['egalites'] = 0;
    }
} else {
// Si on est pas sur un entre deux tours alors on est forcément sur un tour de jeu donc on peut traiter le formulaire ici:
// On vérifie si le formulaire est envoyé avec isset()
    if (isset($_POST)) {
//Pour que l'ordi choisisse aléatoirement entre les 3 choix, on crée le tableau, on génère un index aléatoire $i et on pioche
//dans le tableau l'élément qui se trouve à la position $i.
        $ordi = $choices[$i];
        $joueur = $_POST['usrchoice'];
        echo 'vous avez joué : ' . $joueur;
        echo '</br>';
        echo "L'ordi a joué : " . $ordi;
        echo '</br>';
        if ($ordi == $joueur) {
            echo "Égalité!";
//#Pour la session
// Si les deux on joué la même chose alors on est sur une égalité donc j'incrémente egalites de 1 et je décremente 
// le nb de tours de 1
            $_SESSION['egalites']++;
            $_SESSION['nb_tours']--;
        } elseif ($ordi == "pierre" && $joueur == "feuille") {
            echo "Gagné!";
            //Pour la session
            $_SESSION['win_tours_usr']++;
            $_SESSION['nb_tours']--;
        } elseif ($ordi == "pierre" && $joueur == "ciseaux") {
            echo "Perdu!";
            //Pour la session
            $_SESSION['win_tours_ordi']++;
            $_SESSION['nb_tours']--;
        } elseif ($ordi == "feuille" && $joueur == "pierre") {
            echo "Perdu!";
            $_SESSION['win_tours_ordi']++;
            $_SESSION['nb_tours']--;
        } elseif ($ordi == "feuille" && $joueur == "ciseaux") {
            echo "Gagné!";
            //Pour la session
            $_SESSION['win_tours_usr']++;
            $_SESSION['nb_tours']--;
        } elseif ($ordi == "ciseaux" && $joueur == "feuille") {
            echo "Perdu!";
            //Pour la session
            $_SESSION['win_tours_ordi']++;
            $_SESSION['nb_tours']--;
        } elseif ($ordi == "ciseaux" && $joueur == "pierre") {
            echo "Gagné!";
            //Pour la session
            $_SESSION['win_tours_usr']++;
            $_SESSION['nb_tours']--;
        }

        echo '<br>';
        echo $nb_tours;
        echo '<br>';
        echo "Vous avez gagné :" . $_SESSION['win_tours_usr'];
        echo '<br>';
        echo "L'ordi a gagné :" . $_SESSION['win_tours_ordi'];
        echo '<br>';
        echo 'nb manches :' . $_SESSION['nb_manches'];
    }
}

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <title>Pierre Feuille Ciseaux</title>

    <form action="" method="post">
        <button value="<?= $pierre ?>" name="usrchoice"><?= $pierre ?></button>
        <button value="<?= $feuille ?>" name="usrchoice"><?= $feuille ?></button>
        <button value="<?= $ciseaux ?>" name="usrchoice"><?= $ciseaux ?></button>
    </form>


</head>

<body>

</body>

</html>